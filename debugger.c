/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "./stack.h"
#include "./list.h"

#define MEMSIZE 30000

static void print_help_overview(void);
static void print_help_b(void);
static void print_help_e(void);
static void print_help_g(void);
static void print_help_m(void);
static void print_help_p(void);
static void print_help_grave_accent(void);
static void unknown_command(void);
static void print_intro(void);
static void do_help(const char *const line, const ssize_t bytes);
static void do_print(const char *const line, const ssize_t bytes, const char
    *const code_begin, const char *code);
static void do_goto(const char *const line, const ssize_t bytes, const char
    *const code_begin, const char **code);
static int do_next(const ssize_t bytes, const char **code, Stack ***stack,
    int **d, const int *d_min, const int *d_max);
static void do_run(const ssize_t bytes, const char *const code_begin,
    const char **code, Stack ***stack, List *breakpoints, int **d,
    const int *d_min, const int *d_max);
static void do_execute(const char *const line, const ssize_t bytes, int **d,
    const int *d_min, const int *d_max);
static void do_memory(const char *const line, const ssize_t bytes, int **d,
    int *d_min, int *d_max);
static void print_memory(int *d_min, int *d_max);
static void print_part_of_memory(int *from, int *to, int lnum);
static void execute_one_command(char c, int **d, const int *d_min,
    const int *d_max);
static int is_brainfuck_instruction(const char c);
static void print_part_of_code(const char *const code_begin, const char *from,
    const char *to);
static void do_reset(const char *const line, const ssize_t bytes,
    const char *const code_begin, const char **code, Stack ***stack,
    List **breakpoints, int **d, int *d_min, const int *d_max);
static void do_breakpoint(const char *const line, const ssize_t bytes,
    const char *const code_begin, const char *code, List **breakpoints);
static void toggle_breakpoint(const char *const code_begin,
    List **breakpoints, const char *breakpoint);
static void list_breakpoints(const char *const code_begin, List *breakpoints);
static int search_forward(const char c, const char *code);
static int search_backward(const char c, const char *code,
    const char *const code_begin);

/* brainfuck debugger */
int debug(const char *code) {/*{{{*/
  char *line = NULL;
  const char *const code_begin = code;
  size_t len = 0;
  ssize_t bytes = 0;
  /* pointer to data memory */
  int *d = NULL;
  int *d_max, *d_min;
  /* initialization of jump stack */
  Stack *_s = NULL;
  Stack **stack = &_s;
  /* Initialization of list of breakpoints */
  List *breakpoints = NULL;
  /* create a clear data memory with 0 */
  int *const mem = (int *) malloc(MEMSIZE * sizeof(int));
  if (mem == NULL) {
    perror("malloc");
    return 1;
  }
  memset(mem, 0, (size_t) MEMSIZE * sizeof(int));
  /* 'd' pointing to first cell in mem */
  d = mem;
  d_max = mem + MEMSIZE - 1;
  d_min = d;

  print_intro();
cycle:
  putchar(':');
  fflush(stdout);
  bytes = getline(&line, &len, stdin);
  if (bytes == -1 || bytes == 0) {
    /* ^D */
    puts("");
    goto end;
  } else if (bytes == 1) {
    /* no command given */
    ;
  } else if (line[0] == 'q' && bytes == 2) {
    /* 'quit' command */
    goto end;
  } else if (line[0] == 'h' || line[0] == '?') {
    /* 'help' command */
    do_help(line, bytes);
  } else if (line[0] == 'p') {
    /* 'print' command */
    do_print(line, bytes, code_begin, code);
  } else if (line[0] == 'g') {
    /* 'goto' command */
    do_goto(line, bytes, code_begin, &code);
  } else if (line[0] == 'n') {
    /* 'next' command */
    (void) do_next(bytes, &code, &stack, &d, d_min, d_max);
    printf("\t%u : %c\n", (unsigned) (code - code_begin), *code);
  } else if (line[0] == 'r') {
    /* 'run' command */
    do_run(bytes, code_begin, &code, &stack, breakpoints, &d, d_min, d_max);
  } else if (line[0] == 'e') {
    /* 'execute' command */
    do_execute(line, bytes, &d, d_min, d_max);
  } else if (line[0] == 'm') {
    /* 'memory' command */
    do_memory(line, bytes, &d, d_min, d_max);
  } else if (line[0] == 'b') {
    /* 'breakpoint' command */
    do_breakpoint(line, bytes, code_begin, code, &breakpoints);
  } else if (line[0] == '`') {
    do_reset(line, bytes, code_begin, &code, &stack, &breakpoints,
        &d, d_min, d_max);
  } else {
    /* unknown command given */
    unknown_command();
  }
  goto cycle;

end:
  if (line)
    free(line);
  if (mem)
    free(mem);

  return 0;
}/*}}}*/

static void do_memory(const char *const line, const ssize_t bytes, int **d,
    int *d_min, int *d_max) {/*{{{*/
  if (bytes == 2) {
    printf("%u:%d", (unsigned)(*d - d_min), **d);
    if (20 <= **d && **d <= 126)
      printf("(%c)", **d);
    printf("\n");
  } else if (line[1] != ' ' || bytes == 3) {
    unknown_command();
  } else if (line[2] == '%') {
    if (bytes == 4) {
      print_memory(d_min, d_max);
    } else {
      unknown_command();
    }
  } else if (line[2] == '+' || line[2] == '-' || line[2] == '@') {
    const int sign = line[2] == '+' ? +1 : -1;
    int num = 0, i = 3;
    if (!isdigit(line[3])) {
      unknown_command();
    } else {
      do {
        num = num * 10 + (line[i] - '0');
      } while (isdigit(line[++i]));
      if (line[i] != '\n') {
        unknown_command();
      } else if (line[2] == '@') {
        int *e = *d + num;
        int *f = *d - num;
        if (e > d_max) {
          printf("You hit the end of the memory.\n");
          e = d_max;
        }
        if (f < d_min) {
          printf("You hit the begin of the memory.\n");
          f = d_min;
        }
        print_part_of_memory(f, e, (int) ((f - e - 1) /2));
      } else {
        int *e = *d + (num * sign);
        if (e > d_max) {
          printf("You hit the end of the memory.\n");
          e = d_max;
        } else if (e < d_min) {
          printf("You hit the begin of the memory.\n");
          e = d_min;
        }
        if (*d < e) {
          print_part_of_memory(*d, e, 0);
        } else {
          print_part_of_memory(e, *d, (int) -(*d - e));
        }
      }
    }
  } else if (line[2] == '<') {
    int from = 0, to = 0, i = 3;
    if (!isdigit(line[i])) {
      unknown_command();
      return;
    }
    do {
      from = from * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i++] != ',' || !isdigit(line[i])) {
      unknown_command();
      return;
    }
    do {
      to = to * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i++] != '>' || line[i] != '\n') {
      unknown_command();
      return;
    }
    if (from >= to) {
      puts("Invalid range.");
      return;
    }
    if (to > (int) (d_max - d_min)) {
      printf("You hit the end of the memory.\n");
      to = (int) (d_max - d_min);
    }
    print_part_of_memory(d_min + from, d_min + to, from);
  } else if (line[2] == '>') {
    int i = 3, address = 0, data = 0;
    if (bytes >= 7 && isdigit(line[3])) {
      do {
        address = address * 10 + (line[i] - '0');
      } while (isdigit(line[++i]));
      if (line[i++] != ' ' || !isdigit(line[i])) {
        unknown_command();
      } else {
        do {
          data = data * 10 + (line[i] - '0');
        } while (isdigit(line[++i]));
        if (line[i] != '\n') {
          unknown_command();
        } else {
          if (address > (int) (d_max - d_min)) {
            printf("Cannot write at address %d. Out of bounds.\n", address);
          } else {
            *(d_min + address) = data;
          }
        }
      }
    } else {
      unknown_command();
    }
  } else if (isdigit(line[2])) {
    int num = 0, i = 2;
    do {
      num = num * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
    } else {
      if (num > (int) (d_max - d_min)) {
        puts("Given address is outside the memory.");
      } else {
        printf("%d:%d", num, *(d_min + num));
        if (20 <= *(d_min + num) && *(d_min + num) <= 126)
          printf("(%c)", *(d_min + num));
        printf("\n");
      }
    }
  } else if (line[2] == '&') {
    int num = 0, i = 3;
    do {
      num = num * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
    } else {
      if (num > (int) (d_max - d_min)) {
        puts("Given address is outside the memory.");
      } else {
        *d = d_min + num;
      }
    }
  } else {
    unknown_command();
  }
}/*}}}*/

static void print_part_of_memory(int *from, int *to, int lnum) {/*{{{*/
  int *d = from;
  int values = 0;
  if (from > to) {
    from = to;
    to = d;
    d = from;
  }
  while (d <= to) {
    if (*d) {
      if (values++) printf(", ");
      printf("%d:%d", lnum, *d);
      if (20 <= *d && *d <= 126)
        printf("(%c)", *d);
    }
    lnum++;
    d++;
  }
  if (values > 0) printf("\n");
}/*}}}*/

static void print_memory(int *d_min, int *d_max) {/*{{{*/
  int *d = d_min;
  unsigned n = 0, values = 0;
  if (d_min > d_max) {
    d_min = d_max;
    d_max = d;
    d = d_min;
  }
  while (d <= d_max) {
    if (*d) {
      if (values++) printf(", ");
      printf("%u:%d", n, *d);
      if (20 <= *d && *d <= 126)
        printf("(%c)", *d);
    }
    n++;
    d++;
  }
  if (values > 0) printf("\n");
}/*}}}*/

static void do_execute(const char *const line, const ssize_t bytes, int **d,
    const int *d_min, const int *d_max) {/*{{{*/
  if (bytes != 4 || line[1] != ' ') {
    unknown_command();
    return;
  }
  execute_one_command(line[2], d, d_min, d_max);
}/*}}}*/

static void execute_one_command(char c, int **d, const int *d_min,
    const int *d_max) {/*{{{*/
  switch (c) {
    case '>':
      if (((*d) + 1) < d_max) {
        *d = *d + 1;
      } else {
        printf("Cannot increase data pointer because ");
        printf("it would be out of bounds.\n");
        return;
      }
      break;
    case '<':
      if (((*d) - 1) >= d_min) {
        *d = *d - 1;
      } else {
        printf("Cannot decrease data pointer because ");
        printf("it would be out of bounds.\n");
        return;
      }
      break;
    case '+':
      ++**d;
      break;
    case '-':
      --**d;
      break;
    case '.':
      (void) putchar(**d);
      (void) putchar('\n');
      break;
    case ',':
      **d = getchar();
      while (getchar() != '\n');
      break;
    case '[':
    case ']':
      printf("Cannot execute instruction '%c'.\n", c);
      break;
    default:
      printf("Unknown instruction '%c'.\n", c);
      break;
  }
}/*}}}*/

static void do_run(const ssize_t bytes, const char *const code_begin,
    const char **code, Stack ***stack, List *breakpoints, int **d,
    const int *d_min, const int *d_max) {/*{{{*/
  int retval = 0;
  if (bytes != 2) {
    unknown_command();
    return;
  }
  retval = do_next(bytes, code, stack, d, d_min, d_max);
  while (retval != 1) {
    if (list_contains(breakpoints, *code)) {
      printf("\nBreak at %u.\n", (unsigned) (*code - code_begin));
      break;
    } else {
      retval = do_next(bytes, code, stack, d, d_min, d_max);
    }
  }
}/*}}}*/

/* if do_next returns 1, no instructions follows */
static int do_next(const ssize_t bytes, const char **code, Stack ***stack,
    int **d, const int *d_min, const int *d_max) {/*{{{*/
  int last = 0;
  if (bytes != 2) {
    unknown_command();
    return 1;
  }
  if (*((*code)+1) != '\0') {
    last = 0;
  } else {
    puts("Last instruction executed");
    last = 1;
  }

  switch(**code) {
    case '>':
      if ((*d + 1) < d_max) {
        ++*d;
      } else {
        printf("Instruction '>' is trying to increase data pointer");
        printf(" out of bounds.\nInstruction execution succesfuly failed.\n");
        return 1;
      }
      break;
    case '<':
      if ((*d - 1) >= d_min) {
        --*d;
      } else {
        printf("Instruction '<' is trying to decrease data pointer out of");
        printf(" bounds.\nInstruction execution succesfuly failed.\n");
        return 1;
      }
      break;
    case '+':
      ++**d;
      break;
    case '-':
      --**d;
      break;
    case '.':
      (void) putchar(**d);
      break;
    case ',':
      **d = getchar();
      break;
    case '[':
      if (**d) {
        stack_push(*stack, *code);
      } else {
        /* skip body of the cycle */
        int i = 1;
        while (i) {
          switch (*++*code) {
            case '[': i++; break;
            case ']': i--; break;
            case '\0':
              printf("While searching for matching instruction ']', end of");
              printf(" code was reached. You are now at the end of the code.\n");
              return 1;
              break;
            default:
              break;
          }
        }
      }
      break;
    case ']':
      *code = stack_pop(*stack);
      if (!last) --*code;
      break;
    default:
      /* ignoring non-code */
      break;
  }

  if (!last) {
    /* move to next instruction if current wasn't the last one */
    ++*code;
  }
  return last;
}/*}}}*/

static void do_goto(const char *const line, const ssize_t bytes, const char
    *const code_begin, const char **code) {/*{{{*/
  int sign = 0;
  int arg;
  /* goto command format: 'g|g +<num>|g -<num>|g <num>' */
  if (bytes < 4
      || line[1] != ' '
      || !(
        ((line[2] == '+' || line[2] == '-') && bytes > 3 && isdigit(line[3]))
        || isdigit(line[2]))
      ) {
    unknown_command();
    return;
  }
  if (line[2] == '+') sign = 1;
  if (line[2] == '-') sign = -1;
  if (sign) {
    /* goto with relative address */
    const char *newcode = NULL;
    arg = atoi(line+3);
    if (sign > 0) newcode = *code + arg;
    if (sign < 0) newcode = *code - arg;
    if (*newcode != '\0') {
      *code = newcode;
    } else {
      puts("no cookies there :-/");
    }
  } else {
    /* goto with absolute address */
    arg = atoi(line+2);
    if (code_begin[arg] != '\0') {
      *code = (char *) code_begin + arg;
    } else {
      puts("no cookies there :-/");
    }
  }
}/*}}}*/

static void do_print(const char *const line, const ssize_t bytes, const char
    *const code_begin, const char *code) {/*{{{*/
  if (bytes == 2) {
    printf("\t%u: %c\n", (unsigned) (code - code_begin), *code);
  } else if (line[1] != ' ' || bytes == 3) {
    unknown_command();
  } else if (isdigit(line[2])) {
    /* absolut address */
    int arg = 0, i = 2;
    do {
      arg = arg*10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
    } else {
      if (code_begin[arg] != '\0') {
        printf("\t%u : %c\n", (unsigned)arg, code_begin[arg]);
      } else {
        puts("no cookies there :-/");
      }
    }
  } else if (line[2] == '+' || line[2] == '-' || line[2] == '@') {
    const int sign = line[2] == '+' ? +1 : -1;
    int num = 0, i = 3;
    if (!isdigit(line[3])) {
      unknown_command();
      return;
    }
    do {
      num = num * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
      return;
    }
    if (line[2] == '@') {
      char *from = (char *) code - num;
      char *to = (char *) code + num;
      if (from < code_begin)
        from = (char *) code_begin;
      print_part_of_code(code_begin, from, to);
    } else {
      char *to = (char *) code + (num * sign);
      if (to < code_begin)
        to = (char *) code_begin;
      if (to < code) {
        print_part_of_code(code_begin, to, code);
      } else if (to >= code) {
        print_part_of_code(code_begin, code, to);
      }
    }
  } else if (line[2] == '<') {
    int from = 0, to = 0, i = 3;
    if (!isdigit(line[i])) {
      unknown_command();
      return;
    }
    do {
      from = from * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i++] != ',' || !isdigit(line[i])) {
      unknown_command();
      return;
    }
    do {
      to = to * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i++] != '>' || line[i] != '\n') {
      unknown_command();
      return;
    }
    if (from >= to) {
      puts("Invalid range.");
      return;
    }
    print_part_of_code(code_begin, code_begin + from, code_begin + to);
  } else if (line[2] == '%') {
    const char *c = code_begin;
    char prev = '\0';
    int count = 0, indent = 0, i = 0;
    while (*c) {
      if (*c == prev) {
        putchar(prev);
        if (count == 4) {
          printf(" ");
          count = 0;
        } else {
          count++;
        }
      } else if (is_brainfuck_instruction(*c)) {
        if (*c == ']') indent--;
        printf("\n\t%3u: ", (unsigned) (c - code_begin));
        for (i = indent; i > 0; i--) printf("\t");
        printf("%c", *c);
        if (*c == '[') indent++;
        prev = *c;
        count = 1;
      }
      c++;
    }
    printf("\n");
  } else {
    unknown_command();
  }
}/*}}}*/

static void do_reset(const char *const line, const ssize_t bytes,
    const char *const code_begin, const char **code, Stack ***stack,
    List **breakpoints, int **d, int *d_min, const int *d_max) {/*{{{*/
  if (bytes != 4 || line[1] != ' ') {
    unknown_command();
    return;
  }
  switch (line[2]) {
    case 'm': {
        int *x = d_min;
        memset(x, 0, (size_t) (d_max - d_min + 1) * sizeof(int));
      }
      break;
    case 'p':
      *d = d_min;
      break;
    case 'c':
      *code = code_begin;
      break;
    case 'b':
      list_purge(breakpoints);
      break;
    case 'A': {
        int *x = d_min;
        memset(x, 0, (size_t) (d_max - d_min + 1) * sizeof(int));
      }
      *code = code_begin;
      *d = d_min;
      while (!stack_empty(*stack))
        stack_pop(*stack);
      list_purge(breakpoints);
      break;
    default:
      unknown_command();
      break;
  }
}/*}}}*/

static void do_breakpoint(const char *const line, const ssize_t bytes,
    const char *const code_begin, const char *code, List **breakpoints) {/*{{{*/
  if (bytes == 2) {
    list_breakpoints(code_begin, *breakpoints);
  } else if (bytes == 3 || line[1] != ' ') {
    unknown_command();
  } else if (isdigit(line[2])) {
    int num = 0, i = 2;
    do {
      num = num * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
    } else {
      toggle_breakpoint(code_begin, breakpoints, code_begin + num);
    }
  } else if (line[2] == '+' || line[2] == '-') {
    int sign = line[2] == '+' ? +1 : -1;
    int num = 0, i = 3;
    do {
      num = num * 10 + (line[i] - '0');
    } while (isdigit(line[++i]));
    if (line[i] != '\n') {
      unknown_command();
    } else {
      toggle_breakpoint(code_begin, breakpoints, code + (num * sign));
    }
  } else if (line[2] == '/') {
    char c = line[3];
    if (c == '\n') {
      unknown_command();
    } else {
      int position = search_forward(c, code);
      if (position == -1) {
        printf("Character '%c' not found.\n", c);
      } else {
        toggle_breakpoint(code_begin, breakpoints, code + position);
      }
    }
  } else if (line[2] == '\\') {
    char c = line[3];
    if (c == '\n') {
      unknown_command();
    } else {
      int position = search_backward(c, code, code_begin);
      if (position == -1) {
        printf("Character '%c' not found.\n", c);
      } else {
        toggle_breakpoint(code_begin, breakpoints, code - position);
      }
    }
  } else {
    unknown_command();
  }
}/*}}}*/

static int search_forward(const char c, const char *code) {/*{{{*/
  int position = 0;
SEARCH_FORWARDS_LOOP:
  position++;
  if (*(code + position) == c) {
    return position;
  } else if (*(code + position) == '\0') {
    return -1;
  }
  goto SEARCH_FORWARDS_LOOP;
}/*}}}*/

static int search_backward(const char c, const char *code,
    const char *const code_begin) {/*{{{*/
  int position = 0;
SEARCH_BACKWARDS_LOOP:
  position++;
  if ((code - position) < code_begin) {
    return -1;
  } else if (*(code - position) == c) {
    return position;
  }
  goto SEARCH_BACKWARDS_LOOP;
}/*}}}*/

static void list_breakpoints(const char *const code_begin, List *breakpoints) {/*{{{*/
  while (breakpoints != NULL) {
    printf("\t%3u : %c\n", (unsigned) (breakpoints->address - code_begin),
        *(breakpoints->address));
    breakpoints = breakpoints->next;
  }
}/*}}}*/

static void toggle_breakpoint(const char *const code_begin,
    List **breakpoints, const char *breakpoint) {/*{{{*/
  if (breakpoint < code_begin) {
    puts("Cannot set breakpoint before begin of the code.");
  } else {
    if (list_contains(*breakpoints, breakpoint)) {
      printf("Breakpoint at %u deativated.\n",
          (unsigned) (breakpoint - code_begin));
        list_rm(breakpoints, breakpoint);
    } else {
      printf("Breakpoint at %u activated.\n",
          (unsigned) (breakpoint - code_begin));
      list_add(breakpoints, breakpoint);
    }
  }
}/*}}}*/

static void print_part_of_code(const char *const code_begin, const char *from,
    const char *to) {/*{{{*/
  const char *c = from;
  while (*c != '\0' && c <= to) {
    if (is_brainfuck_instruction(*c))
      printf("\t%u : %c\n", (unsigned) (c - code_begin), *c);
    c++;
  }
  return;
}/*}}}*/

static int is_brainfuck_instruction(const char c) {/*{{{*/
  switch (c) {
    case '+':
    case '-':
    case '>':
    case '<':
    case '[':
    case ']':
    case ',':
    case '.':
      return 1;
    default:
      return 0;
  }
}/*}}}*/

static void do_help(const char *const line, const ssize_t bytes) {/*{{{*/
  if (bytes == 2) {
    print_help_overview();
  } else if (bytes == 4 && line[1] == ' ') {
    switch (line[2]) {
      case 'p': print_help_p(); break;
      case 'm': print_help_m(); break;
      case 'b': print_help_b(); break;
      case 'e': print_help_e(); break;
      case 'g': print_help_g(); break;
      case '`': print_help_grave_accent(); break;
      default: printf("No help for '%c'\n", line[2]);
    }
  } else {
    /* unknown command given (but it begins with 'h..') */
    unknown_command();
  }
}/*}}}*/

static void print_intro(void) {/*{{{*/
  puts("d8gbd - dum8d0gs brainfuck debugger");
  puts("Copyright (c) 2010 Martin Kopta <martin@kopta.eu>");
  puts("Use 'h' or '?' to get list of commands.");
}/*}}}*/

static void unknown_command(void) {/*{{{*/
  puts("Unknown command. Use 'h' or '?' to get list of commands.");
}/*}}}*/

static void print_help_overview(void) {/*{{{*/
  puts("commands overview:");
  puts("h or ?    this help");
  puts("r         run till end or breakpoint");
  puts("p         print code");
  puts("m         memory operations");
  puts("n         next instruction (step)");
  puts("b         breakpoints management");
  puts("g         goto instruction");
  puts("e         execute instruction");
  puts("`         reset memory/code/pointer/all");
  puts("q or ^D   quit");
  puts("\nUse 'h <letter>' for detailed description (e.g. 'h p')");
}/*}}}*/

static void print_help_p(void) {/*{{{*/
  puts("p         print current instruction");
  puts("p %       print full source");
  puts("p 2       print second instruction in code");
  puts("p +4      print 4 instructions after");
  puts("p -4      print 4 instructions before");
  puts("p @4      print 4 instructions before and after");
  puts("p <4,10>  print from fourth instruction in code till 10th");
}/*}}}*/

static void print_help_m(void) {/*{{{*/
  puts("m         show data pointer (DP)");
  puts("m %       dump memory (only nonzero values)");
  puts("m 2       show value in second cell");
  puts("m +4      show next four cells from DP");
  puts("m -4      show prev four cells from DP");
  puts("m @4      show four cells before and after DP");
  puts("m <4,10>  show cells 4, 5, 6, 7, 8, 9, 10");
  puts("m >2 72   store value 72 into cell 2");
  puts("m &10     set data pointer to address 10");
}/*}}}*/

static void print_help_b(void) {/*{{{*/
  puts("b         print all breakpoints");
  puts("b 57      toggle breakpoint on 57th instruction");
  puts("b +10     toggle breakpoint on next 10th instruction");
  puts("b -10     toggle breakpoint on prev 10th instruction");
  puts("b /c      toggle breakpoint at first occurence of character 'c'");
  puts("          (search forward)");
  puts("b \\c      toggle breakpoint at first occurence of character 'c'");
  puts("          (search backward)");
}/*}}}*/

static void print_help_g(void) {/*{{{*/
  puts("g -5      relative jump to fifth instruction before");
  puts("g  0      absolute jump to first instruction");
  puts("g +5      relative jump to fifth instruction after");
}/*}}}*/

static void print_help_e(void) {/*{{{*/
  puts("e >       move data pointer to the right");
  puts("e <       move data pointer to the left");
  puts("e +       increment value under DP");
  puts("e -       decrement value under DP");
  puts("e ,       read value and store under DP");
  puts("e .       print value under DP");
}/*}}}*/

static void print_help_grave_accent(void) {/*{{{*/
  puts("` m   reset memory (erase)");
  puts("` p   reset data pointer (to address 0)");
  puts("` c   reset code (start from beginning)");
  puts("` b   remove all breakpoints");
  puts("` A   reset all");
}/*}}}*/

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
