/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include "./stack.h"
#include "./debugger.h"

#define MEMSIZE 30000

/* brainfuck interpreter */
int interpret(const char *f) {

  /* pointer to data memory */
  int *d;
  /* initialization of jump stack */
  Stack *_ = NULL;
  Stack **stack = &_;

  /* create a clear data memory with 0 */
  int *mem = (int *) malloc(MEMSIZE * sizeof(int));
  if (mem == NULL) {
    perror("malloc");
    return 1;
  }
  memset(mem, 0, (size_t) MEMSIZE);
  /* 'd' pointing to first cell in mem */
  d = mem;

  /* interpreting program */
  while (*f != '\0') {
    switch (*f) {
      case '>':
        ++d;
        if (d > (mem + MEMSIZE - 1)) {
          fprintf(stderr, "Data pointer points out of memory (upper bound).\n");
          free(mem);
          return 1;
        }
        break;
      case '<':
        --d;
        if (d < mem) {
          fprintf(stderr, "Data pointer points out of memory (lower bound).\n");
          free(mem);
          return 1;
        }
        break;
      case '+':
        ++*d;
        break;
      case '-':
        --*d;
        break;
      case '.':
        putchar(*d);
        break;
      case ',':
        *d = getchar();
        if (*d == EOF) {
          free(mem);
          return 0;
        }
        break;
      case '[':
        if (*d) {
          stack_push(stack, f);
        } else {
          /* skip body of the cycle */
          int i = 1;
          while (i) {
            switch (*++f) {
              case '[': i++; break;
              case ']': i--; break;
              case '\0':
                fprintf(stderr, "Unmatched parenthesis\n");
                free(mem);
                return 1;
                break;
              default:
                break;
            }
          }
        }
        break;
      case ']':
        f = stack_pop(stack);
        f--;
        break;
      default:
        break;
    }
    f++;
  }
  puts("");

  /* freeiyng data memory */
  free(mem);

  return 0;
}

int main(int argc, char **argv) {
  int fd = 0, pagesize, retval = 0;
  char *code = NULL;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <source_file>\n", *argv);
    return 1;
  }

  fd = open(*++argv, O_RDONLY);
  if (fd == -1) {
    perror("open");
    return 1;
  }

  pagesize = (int) sysconf(_SC_PAGESIZE);
  if (pagesize == -1) {
    perror("sysconf");
    (void) close(fd);
    return 1;
  }

  code = mmap(NULL, (size_t) pagesize, PROT_READ, MAP_PRIVATE, fd, (size_t) 0);
  if (code == MAP_FAILED) {
    perror("mmap");
    (void) close(fd);
    return 1;
  }

  if (strstr(*--argv, "d8gbd") != NULL) {
    if (debug(code) == 1) {
      retval = 1;
    }
  } else if (interpret(code) == 1) {
    retval = 1;
  }

  if (munmap(code, (size_t) pagesize) == -1) {
    perror("munmap");
    retval = 1;
  }

  if (close(fd) == -1) {
    perror("close");
    retval = 1;
  }

  return retval;
}
