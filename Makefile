CC        = gcc
#CFLAGS    = -ansi
#CFLAGS   += -Wall -Wextra -Wconversion -pedantic
#CFLAGS   += -DDEBUG -ggdb3
LN        = ln
OBJS      = d8gbi.o debugger.o stack.o list.o

all: d8gbi

d8gbi: $(OBJS)
	$(CC) $(CFLAGS) -o d8gbi $(OBJS)
	$(LN) d8gbi d8gbd

d8gbi.o: d8gbi.c stack.h debugger.h
	$(CC) $(CFLAGS) -c -o d8gbi.o d8gbi.c

debugger.o: debugger.c debugger.h stack.h
	$(CC) $(CFLAGS) -c -o debugger.o debugger.c

stack.o: stack.h
	$(CC) $(CFLAGS) -c -o stack.o stack.c

list.o: list.h
	$(CC) $(CFLAGS) -c -o list.o list.c

test: d8gbi
	./d8gbd samples/hw.bf

clean:
	rm -f d8gbi d8gbd
	rm -f $(OBJS)
