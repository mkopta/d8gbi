/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./stack.h"

int stack_empty(Stack **head) {/*{{{*/
  return *head == NULL;
}/*}}}*/

void stack_push(Stack **head, const char *address) {/*{{{*/
  Stack *node = (Stack *) malloc(sizeof(Stack));
  if (node == NULL) {
    perror("malloc");
    abort();
  } else {
    node->address = address;
    node->next = stack_empty(head) ? NULL : *head;
    *head = node;
  }
}/*}}}*/

const char *stack_pop(Stack **head) {/*{{{*/
  const char *address;
  if (stack_empty(head)) {
    fputs("Stack underflow\n", stderr);
    abort();
  } else {
    Stack *top = *head;
    address = top->address;
    *head = top->next;
    free(top);
    return address;
  }
}/*}}}*/

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
