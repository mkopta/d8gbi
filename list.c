/* Copyright (c) <2010> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************  }}}1*/

#include <stdio.h>
#include <stdlib.h>
#include "./list.h"

void list_add(List **head, const char *address) {/*{{{*/
  List *element;
  if (list_contains(*head, address))
    return;
  element = (List *) malloc(sizeof(List));
  if (element == NULL) {
    perror("malloc");
    abort();
  } else {
    element->address = address;
    element->next = list_empty(*head) ? NULL : *head;
    *head = element;
  }
}/*}}}*/

void list_rm(List **head, const char *address) {/*{{{*/
  List *prev = NULL, *now = *head;

  while (now != NULL) {
    if (now->address == address) {
      if (prev != NULL) {
        prev->next = now->next;
      } else {
        *head = now->next;
      }
      free(now);
      break;
    } else {
      prev = now;
      now = now->next;
    }
  }
}/*}}}*/

int list_contains(List *head, const char *address) {/*{{{*/
  while (head != NULL) {
    if (head->address == address)
      return 1;
    else
      head = head->next;
  }
  return 0;
}/*}}}*/

int list_empty(List *head) {/*{{{*/
  return head == NULL;
}/*}}}*/

void list_purge(List **head) {/*{{{*/
  List *next = *head, *rm = NULL;
  while (next != NULL) {
    rm = next;
    next = next->next;
    free(rm);
  }
  *head = NULL;
}/*}}}*/

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
